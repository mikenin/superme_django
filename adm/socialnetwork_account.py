from django.contrib import admin

from models.socialnetwork_account import SocialnetworkAccount


@admin.register(SocialnetworkAccount)
class SocialnetworkAccountAdmin(admin.ModelAdmin):
    list_display = ['user', 'type_socialnetwork', 'date_updated','date_created']
    ordering = ('-date_created',)

    class Meta:
        model = SocialnetworkAccount