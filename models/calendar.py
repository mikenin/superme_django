from django.db import models
import uuid
from django.db.models.signals import post_save
from django.dispatch import receiver

from service.redis import RedisService

class Calendar(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    date_drinking = models.DateField(verbose_name='Дата', db_index=True)

    is_drinked = models.BooleanField(default=False, verbose_name='Пил')
    user = models.ForeignKey('User', related_name='calendar_user', on_delete=models.CASCADE, verbose_name='Пользователь')

    date_created = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    date_updated = models.DateTimeField(auto_now=True, verbose_name='Дата последнего изменения')

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = "Календарь пил/не пил"
        verbose_name_plural = "Календарь пил/не пил"

@receiver(post_save, sender=Calendar)
def cache_need_update(sender, instance, **kwargs):
    redis = RedisService()
    redis.need_update("{}".format(instance.user.username))
    redis.need_update("all_{}".format(instance.user.username))
    redis.need_update("current_{}".format(instance.user.username))
    redis.need_update("prev_{}".format(instance.user.username))
    redis.need_update("thirty_{}".format(instance.user.username))
    redis.need_update("year_{}".format(instance.user.username))

