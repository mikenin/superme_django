from django.contrib import admin

from models.push_notification import PushNotification


@admin.register(PushNotification)
class PushNotificationAdmin(admin.ModelAdmin):
    list_display = ['user', 'is_push',]
    ordering = ('-date_created',)

    class Meta:
        model = PushNotification