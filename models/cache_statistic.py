from django.db import models
import uuid

from superme_django.constants import TYPE_STATISTICS


class CacheStatistic(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    user = models.ForeignKey('User', related_name='cache_statistic_user', on_delete=models.CASCADE, verbose_name='Пользователь')
    date_starting_drinking = models.DateField(blank=True, null=True, verbose_name='Дата')
    calendar_empty = models.BooleanField(default=False)
    is_often_drinking = models.BooleanField(default=False)
    count_often_drinking = models.IntegerField(default=0)
    with_alcohol = models.IntegerField(default=0)
    without_alcohol = models.IntegerField(default=0)
    amount_day = models.IntegerField(default=0)
    consecutive_with_alcohol = models.IntegerField(default=0)
    consecutive_without_alcohol = models.IntegerField(default=0)
    type_statistics = models.CharField(choices=TYPE_STATISTICS, max_length=150, verbose_name='Тип статистики')

    date_created = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    date_updated = models.DateTimeField(auto_now=True, verbose_name='Дата последнего изменения')

    def __str__(self):
        return f"{self.user.username}"

    class Meta:
        verbose_name = "Кэш статистики"
        verbose_name_plural = "Кэш статистики"

