from rest_framework import serializers

from models.user import User
from superme_django.constants import TYPE_SOCIALNETWORK


class RegisterUserSerializer(serializers.Serializer):
    username = serializers.CharField(allow_blank=True, allow_null=True)
    device_id = serializers.CharField(required=True)
    email = serializers.EmailField(allow_blank=True, allow_null=True)
    first_name = serializers.CharField(allow_blank=True, allow_null=True)
    last_name = serializers.CharField(allow_blank=True, allow_null=True)
    id_account_network = serializers.CharField(allow_blank=True, allow_null=True)