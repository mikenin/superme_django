from rest_framework.response import Response
from rest_framework.views import APIView

from service.custom_is_auth import CustomIsAuthenticated
from service.manage_push_for_user import ManagePushForUserService

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema


class ManagePushForUser(APIView):
    permission_classes = [CustomIsAuthenticated, ]

    @swagger_auto_schema(
        operation_summary='Включение/отключение пушей',
        operation_description="Нужен токен",
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'uid_smartphone': openapi.Schema(type=openapi.TYPE_STRING, description='438YGAORYBVO'),
                'is_push': openapi.Schema(type=openapi.TYPE_BOOLEAN, description='False'),
            }
        ),
        responses={
            "200": openapi.Response(
                description="",
                examples={
                    "application/json": {'message': 'Статус дня обновлен',
                                         'status_code': 200,
                                         'status': 'Success'}
                }
            ),
            "400": openapi.Response(
                description="",
                examples={
                    "application/json": {'message': 'Данные не валидны/Ошибка смены статуса',
                    'status_code': 400,
                    'status': 'Failed'}
                }
            ),

        }
    )

    def post(self, request):
        token = request.headers['Authorization'][7:]

        manage = ManagePushForUserService()
        result = manage.call(request.data, token)

        return Response({'status': result['status'], 'message': result['message']}, status=result['status_code'])