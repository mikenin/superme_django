import redis
import json
from django.core.cache import cache
from uuid import UUID
from superme_django.settings import CACHE_TLL


class RedisService:

    def set(self, key, value):
        data = json.dumps({
            "data": value,
            "need_update": False
        }, default=self.__uuid_convert)
        cache.set(key, data, timeout=CACHE_TLL)
        return True

    def get(self, key):
        value = cache.get(key)
        if not value:
            return None
        data = json.loads(value)
        return data

    def need_update(self, key):
        data = self.get(key)
        if data and not data['need_update']:
            new_data = json.dumps({
                "data": data['data'],
                "need_update": True
            })
            cache.set(key, new_data)

    def __uuid_convert(self, field):
        if isinstance(field, UUID):
            return field.hex