import json
import os

import requests

import psycopg2
import logging


class GetUsersDevice:
    def __init__(self):
        self.conn = psycopg2.connect(dbname=os.getenv('POSTGRES_DB', 'superme'),
                                     user=os.getenv('POSTGRES_USER', 'admin'),
                                     password=os.getenv('POSTGRES_PASSWORD', 'admin'),
                                     host=os.getenv('POSTGRES_HOST', 'localhost'))

    def _query(self):
        cursor = self.conn.cursor()
        cursor.execute("SELECT uid_smartphone FROM models_pushnotification WHERE is_push = TRUE " +
                       "AND user_id NOT IN " +
                       "(SELECT DISTINCT(user_id) FROM models_calendar " +
                       "WHERE date_drinking <= CURRENT_DATE AND date_drinking >= CURRENT_DATE-6) "
                       )

        return cursor.fetchall()


    def call(self):
        return self._query()

class SenderPush:
    def __init__(self):
        self.url = 'https://fcm.googleapis.com/fcm/send'

        self.headers = {'Content-type': 'application/json',
                        'Authorization': 'key={}'.format(os.getenv('FIREBASE_KEY', 'AAAAZoubhyQ:APA91bFRJ-TN08dPvvgIRJhnDqx9YL6QBTpYhapoBtZ8UnhF9MKtvfJnVwVuEtjxhzlc3BuOoqQD7YdSaPsImGunV-k3B7Uk8Keb2kC9PWdPyNw2S7iZE4iUyLuQcp_cs9ZDR5SOP6LF'))
                        }
        self.conn = psycopg2.connect(dbname=os.getenv('POSTGRES_DB', 'superme'),
                                     user=os.getenv('POSTGRES_USER', 'admin'),
                                     password=os.getenv('POSTGRES_PASSWORD', 'admin'),
                                     host=os.getenv('POSTGRES_HOST', 'localhost'))

    def _send_push(self, user):
        data = {
            "notification": {
                "title": 'SuperMe: напоминание',
                "body": 'Открой календарь и сделай записи о прошедшей неделе',
                "sound": None,
                "badge": 1,
            },
            # "content_available": True,
            # "priority": "high",
            # "mutable_content": True,
            "registration_ids": user
        }

        requests.post(self.url, data=json.dumps(data), headers=self.headers)


    def _chunks(self, user_list, chunk_size):
        return [user_list[i:i + chunk_size] for i in range(0, len(user_list), chunk_size)]

    def _convert_tuple_to_string(self, user_list):
        return [t[0] for t in user_list]

    def push(self):
        print('Start push sending')
        users = GetUsersDevice()
        user_devices = users.call()

        if not user_devices:
            return

        chunk_user_list = self._chunks(user_devices, 500)
        print('SEND PUSH')
        for user in chunk_user_list:
            self._send_push(self._convert_tuple_to_string(user))
            # asd = self._convert_tuple_to_string(user)
            # print(asd)
