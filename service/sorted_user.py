from django.db.models import Count

from models.cache_statistic import CacheStatistic
from models.calendar import Calendar
from models.user import User
from datetime import datetime
from service.statistics import StatisticsService
from superme_django.constants import ALL


class SortedUserService:

    def _sorted_login(self, type_order):
        return User.objects.all().order_by(type_order)

    def _sorted_amount_not_drinking(self, type_order):
        result_list = list()
        temp_dict = dict()

        user_list = User.objects.all()

        for item in user_list:
            if CacheStatistic.objects.filter(user=item, type_statistics=ALL).exists():
                cache = CacheStatistic.objects.get(user=item, type_statistics=ALL)
                temp_dict[item] = cache.without_alcohol
            else:
                start_object_date = Calendar.objects.filter(user=item, is_drinked=True).order_by('date_drinking')[:1]
                if start_object_date:
                    start_date = start_object_date[0].date_drinking
                else:
                    start_date = item.date_register.date()

                end_date = datetime.today().date()

                amount_day = (end_date - start_date).days + 1

                amount_day = 1 if amount_day <= 0 else amount_day

                with_alcohol = Calendar.objects.filter(user=item, is_drinked=True,
                                                       date_drinking__range=[start_date, end_date]).count()
                without_alcohol = amount_day - with_alcohol
                temp_dict[item] = without_alcohol


        if type_order == 'count':
            result_list = [k for k, v in sorted(temp_dict.items(), key=lambda item: item[1])]
        else:
            result_list = [k for k, v in sorted(temp_dict.items(), key=lambda item: item[1], reverse=True)]

        return result_list


    def _sorted_amount_drinking(self, type_order):
        result_list = list()
        temp_list = list()
        exist_id_list = list()

        calendar_list = Calendar.objects.filter(is_drinked=True).values('user').annotate(
            count=Count('user')).order_by(type_order)

        for item in calendar_list:
            temp_list.append(User.objects.get(id=item['user']))
            exist_id_list.append(item['user'])

        if type_order == 'count':
            result_list += User.objects.all().exclude(id__in=exist_id_list).order_by('-date_register')
            result_list += temp_list
        else:
            result_list += temp_list
            result_list += User.objects.all().exclude(id__in=exist_id_list).order_by('-date_register')

        return result_list

    def _sorted_consecutive_with_alcohol(self, type_order):
        temp_list = list()

        user_list = User.objects.all()

        for item in user_list:
            if CacheStatistic.objects.filter(user=item, type_statistics=ALL).exists():
                cache = CacheStatistic.objects.get(user=item, type_statistics=ALL)
                temp_list.append(
                    {
                        'user': item,
                        'amount': cache.consecutive_with_alcohol
                    }
                )
            else:
                start_object_date = Calendar.objects.filter(user=item, is_drinked=True).order_by('date_drinking')[:1]
                if not start_object_date:
                    temp_list.append(
                        {
                            'user': item,
                            'amount': 0
                        }
                    )
                else:
                    start_date = start_object_date[0].date_drinking
                    end_date = datetime.today().date()
                    try:
                        end_date = end_date if (end_date - item.date_second_login.date()).days + 1 < 14 \
                            else item.date_second_login.date()
                    except:
                        print('with')

                    amount_day = (end_date - start_date).days + 1
                    amount_day = 1 if amount_day <= 0 else amount_day
                    stat = StatisticsService()
                    stat.user = item
                    temp_list.append(
                        {
                            'user': item,
                            'amount': stat.get_consecutive_with_alcohol(amount_day=amount_day, end_date=end_date)
                        }
                    )

        if type_order == 'count':
            ordering_list = sorted(temp_list, key=lambda x: x['amount'])
        else:
            ordering_list = sorted(temp_list, key=lambda x: x['amount'], reverse=True)

        result_list = list(map(lambda x: x.get('user'), ordering_list))

        return result_list

    def _sorted_consecutive_without_alcohol(self, type_order):
        temp_list = list()

        user_list = User.objects.all()

        for item in user_list:
            if CacheStatistic.objects.filter(user=item, type_statistics=ALL).exists():
                cache = CacheStatistic.objects.get(user=item, type_statistics=ALL)
                temp_list.append(
                    {
                        'user': item,
                        'amount': cache.consecutive_without_alcohol
                    }
                )
            else:
                start_object_date = Calendar.objects.filter(user=item, is_drinked=True).order_by('date_drinking')[:1]
                if not start_object_date:
                    temp_list.append(
                        {
                            'user': item,
                            'amount': 0
                        }
                    )
                else:
                    start_date = start_object_date[0].date_drinking
                    end_date = datetime.today().date()
                    try:
                        end_date = end_date if (end_date - item.date_second_login.date()).days + 1 < 14 \
                            else item.date_second_login.date()
                    except:
                        print('without')


                    amount_day = (end_date - start_date).days + 1
                    amount_day = 1 if amount_day <= 0 else amount_day
                    stat = StatisticsService()
                    stat.user = item
                    temp_list.append(
                        {
                            'user': item,
                            'amount': stat.get_consecutive_without_alcohol(amount_day=amount_day, end_date=end_date)
                        }
                    )

        if type_order == 'count':
            ordering_list = sorted(temp_list, key=lambda x: x['amount'])
        else:
            ordering_list = sorted(temp_list, key=lambda x: x['amount'], reverse=True)

        result_list = list(map(lambda x: x.get('user'), ordering_list))

        return result_list

    def call(self, type_sorted):
        result = None

        if type_sorted == 'date_login_increase':
            result = self._sorted_login('date_login')
        elif type_sorted == 'date_login_decrease':
            result = self._sorted_login('-date_login')
        elif type_sorted == 'date_register_increase':
            result = self._sorted_login('date_register')
        elif type_sorted == 'date_register_decrease':
            result = self._sorted_login('-date_register')
        elif type_sorted == 'amount_drinking_increase':
            result = self._sorted_amount_drinking('count')
        elif type_sorted == 'amount_drinking_decrease':
            result = self._sorted_amount_drinking('-count')
        elif type_sorted == 'amount_not_drinking_increase':
            result = self._sorted_amount_not_drinking('count')
        elif type_sorted == 'amount_not_drinking_decrease':
            result = self._sorted_amount_not_drinking('-count')
        elif type_sorted == 'consecutive_with_alcohol_increase':
            result = self._sorted_consecutive_with_alcohol('count')
        elif type_sorted == 'consecutive_with_alcohol_decrease':
            result = self._sorted_consecutive_with_alcohol('-count')
        elif type_sorted == 'consecutive_without_alcohol_increase':
            result = self._sorted_consecutive_without_alcohol('count')
        elif type_sorted == 'consecutive_without_alcohol_decrease':
            result = self._sorted_consecutive_without_alcohol('-count')

        return result
