from models.calendar import Calendar
from models.user import User
from superme_django.constants import TRANSLATE_MONTH
import jwt
from superme_django import settings


class GetDrinkingCalendarService:

    def call(self, token):
        try:
            user_info = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
            print('user info drinking calendar')
            print(user_info)
            user = User.objects.get(username=user_info['username'])
            drinking_calendar = self.get_calendar(user=user)
            print(user)
            print('drinking calendar')
            return {'message': 'Календарь выпивки подготовлен',
                    'status_code': 200,
                    'status': 'Success',
                    'drinking_calendar': drinking_calendar,
                    }
        except Exception as e:
            print('get drinking calendar')
            print(e)
            return {'message': 'Ошибка подготовки календаря "история пользователя", попробуйте позже',
                    'status_code': 400,
                    'status': 'Failed',
                    'drinking_calendar': None}

    def get_calendar(self, user):
        drinking_list = Calendar.objects.filter(user=user)
        drinking_calendar = dict()

        for item in drinking_list:
            drinking_calendar[
                str(item.date_drinking.day) + "." + str(item.date_drinking.month) + "." + str(item.date_drinking.year)
            ] = item.is_drinked
            # drinking_calendar[
            #     str(item.date_drinking.day) + " " + TRANSLATE_MONTH.get(item.date_drinking.month)
            #     + " " + str(item.date_drinking.year)
            #     ]  = item.is_drinked

        return drinking_calendar