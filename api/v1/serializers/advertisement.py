import os

from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from models.advertisment import AdvertismentBanner


class AdvertismentBannerSerializer(serializers.Serializer):
    banner = SerializerMethodField()

    class Meta:
        model = AdvertismentBanner
        fields = '__all__'

    def get_banner(self, obj):
        return os.getenv('SITE_HOST', 'localhost:8000') + '/media/' + str(obj.banner)