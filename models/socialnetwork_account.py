from django.db import models
import uuid

from superme_django.constants import TYPE_SOCIALNETWORK


class SocialnetworkAccount(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)

    type_socialnetwork = models.CharField(choices=TYPE_SOCIALNETWORK, max_length=150, verbose_name='Тип аккаунта')
    user = models.ForeignKey('User', related_name='socialnetwork_user', on_delete=models.CASCADE, verbose_name='Пользователь')

    id_account_network = models.CharField(max_length=255, blank=True, verbose_name='ID account')
    first_name = models.CharField(max_length=30, blank=True, verbose_name='Имя')
    last_name = models.CharField(max_length=30, blank=True, verbose_name='Фамилия')

    date_created = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    date_updated = models.DateTimeField(auto_now=True, verbose_name='Дата последней авторизации')

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = "Аккаунт в социальных сетях"
        verbose_name_plural = "Аккаунты в социальных сетях"
