from rest_framework.response import Response
from rest_framework.views import APIView

from service.custom_is_auth import CustomIsAuthenticated
from service.get_drinking_calendar import GetDrinkingCalendarService

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema


class GetDrinkingCalendar(APIView):
    permission_classes = [CustomIsAuthenticated, ]

    @swagger_auto_schema(
        operation_summary='Возврат календаря выпивки пользователя',
        operation_description="Нужен токен",
        responses={
            "200": openapi.Response(
                description="",
                examples={
                    "application/json": {
    "status": "Success",
    "message": "Календарь выпивки подготовлен",
    "drinking_calendar": {
        "17.3.2021": True,
        "16.3.2021": False,
        "28.2.2021": True,
    }
}
                }
            ),
            "400": openapi.Response(
                description="",
                examples={
                    "application/json": {'message': 'Ошибка подготовки календаря "история пользователя", попробуйте позже',
                    'status_code': 400,
                    'status': 'Failed',
                    'drinking_calendar': None}
                }
            ),

        }
    )

    def get(self, request):
        token = request.headers['Authorization'][7:]

        calendar = GetDrinkingCalendarService()
        result = calendar.call(token)


        return Response({'status': result['status'],
                         'message': result['message'],
                         'drinking_calendar': result['drinking_calendar'],
                         },
                        status=result['status_code'])
