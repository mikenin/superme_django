from django.shortcuts import render

from api.v1.serializers.user_statistics import UserStatisticsSerializer
from api.v1.serializers.user_statistics_one import UserStatisticsOneSerializer
from models.user import User


def user_info_admin_custom_page(request, username):
    try:
        user = User.objects.get(username=username)
        serializer = UserStatisticsOneSerializer(user, many=False).data

        ctx = {'data': serializer}
        return render(request, 'user_info_admin_custom_page.html', ctx)
    except Exception as e:
        print(e)
        return render(request, 'user_admin_custom_page.html')


