from django.urls import path
from django.conf.urls import url

from api.v1.views.advertisment import Advertisement
from api.v1.views.anonim_user import AnonimAuth
from api.v1.views.apple_auth import AppleAuth
from api.v1.views.auth_login import AuthLogin
from api.v1.views.drinking_user_admin import DrinkingUserAdmin
from api.v1.views.get_calendar import GetCalendar
from api.v1.views.get_drinking_calendar import GetDrinkingCalendar
from api.v1.views.google_auth import GoogleAuth
from api.v1.views.manage_push_for_user import ManagePushForUser
from api.v1.views.save_status_drinking import SaveStatusDrinking
from api.v1.views.social_auth import SocialAuth
from api.v1.views.statistics import Statistics

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from api.v1.views.statistics_all import StatisticsAll
from api.v1.views.statistics_current import StatisticsCurrent

from api.v1.views.statistics_every_day import StatisticsEveredayPopup
from api.v1.views.statistics_prev import StatisticsPrevious
from api.v1.views.statistics_refresh import StatisticsRefresh
from api.v1.views.statistics_thirty import StatisticsThirty
from api.v1.views.statistics_year import StatisticsYear

schema_view = get_schema_view(
   openapi.Info(
      title="SuperMe API",
      default_version='v1',
      description="Описание точек доступа к бэкенду мобильного приложения SuperMe",
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)



urlpatterns = [
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

    path(r'statistics', Statistics.as_view(), name='Статистика для пользователя'),
    path(r'statistics/all', StatisticsAll.as_view(), name='statistics_all'),
    path(r'statistics/year', StatisticsYear.as_view(), name='statistics_year'),
    path(r'statistics/thirty', StatisticsThirty.as_view(), name='statistics_thirty'),
    path(r'statistics/previous', StatisticsPrevious.as_view(), name='statistics_previous'),
    path(r'statistics/current', StatisticsCurrent.as_view(), name='statistics_current'),

    path(r'advertisement', Advertisement.as_view(), name='Рекламный баннер'),

    path(r'statistics/everyday_popup', StatisticsEveredayPopup.as_view(), name='Получение статистики для баннера '
                                                                               'при первом запуске приложения'),
    path(r'statistics/refresh', StatisticsRefresh.as_view(), name='Обнуление статистики пользователя'),

    path(r'drinking/save', SaveStatusDrinking.as_view(), name='Сохраненеи статуса по выпивке'),
    path(r'user/manage/push', ManagePushForUser.as_view(), name='Вкл/выкл пушей для пользователя'),

    path(r'calendar/general', GetCalendar.as_view(), name='Календарь для пользователя'),
    path(r'calendar/drinking', GetDrinkingCalendar.as_view(), name='Календарь выпивания пользователя'),

    path(r'user/drinking/<str:username>', DrinkingUserAdmin.as_view(),
                       name='statistic_user_admin'),

    path(r'auth/anonim', AnonimAuth.as_view(), name='Анонимная регистрация пользователя'),
    # path(r'auth/google', GoogleAuth.as_view(), name='Google регистрация пользователя'),
    # path(r'auth/apple', AppleAuth.as_view(), name='Apple регистрация пользователя'),

    path(r'auth/social', SocialAuth.as_view(), name='Общая регистрация для социалок'),
    path(r'auth/login', AuthLogin.as_view(), name='Сохраняем логин'),

]