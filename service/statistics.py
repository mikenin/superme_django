import jwt
from django.db.models.functions import Lower

from models.calendar import Calendar
from models.user import User
from superme_django import settings

from datetime import datetime, timedelta
from django.db.models import Avg, Count
from dateutil.relativedelta import relativedelta


class StatisticsService:
    def __init__(self):
        self.user = None

    def _get_drinking_list(self, start_date, end_date):
        with_alcohol = Calendar.objects.filter(user=self.user, is_drinked=True,
                                               date_drinking__range=[start_date, end_date]).count()

        without_alcohol = (end_date - start_date).days + 1 - with_alcohol
        return with_alcohol, without_alcohol

    def _get_percent(self, start_date, end_date, with_alcohol):
        sum_elem = 0
        amount_elem = 0
        calendar = Calendar.objects.filter(is_drinked=True, date_drinking__range=[start_date, end_date]
                                           ).exclude(user=self.user) \
            .values('user__username').annotate(count=Count('user'))

        if not calendar:
            amount_elem = 1
            sum_elem = 1
        else:
            amount_elem = len(calendar)
            for item in calendar:
                sum_elem += item['count']

        average_users_drinking = round(sum_elem / amount_elem)

        is_often_drinking = True if average_users_drinking < with_alcohol else False

        percent = round((with_alcohol * 100) / average_users_drinking)

        if average_users_drinking == with_alcohol:
            count_often_drinking = 10  # для маркетинга, хотя тут должен быть 0
        else:
            count_often_drinking = percent if percent <= 100 else 100

        return is_often_drinking, count_often_drinking

    def get_consecutive_with_alcohol(self, amount_day, end_date):
        try:
            result_consecutive = 0
            temp_alcohol = 0
            temp_list_consecutive_with_alcohol = list()
            for item in range(0, amount_day, 1):
                current_date_loop = end_date - timedelta(days=item if item > 0 else 0)
                if Calendar.objects.filter(user=self.user, date_drinking=current_date_loop, is_drinked=True).exists():
                    temp_alcohol += 1
                else:
                    if temp_alcohol > 0:
                        if result_consecutive < temp_alcohol:
                            result_consecutive = temp_alcohol
                        # temp_list_consecutive_with_alcohol.append(temp_alcohol)
                    temp_alcohol = 0

                if item+1 == amount_day and temp_alcohol > 0:
                    # temp_list_consecutive_with_alcohol.append(temp_alcohol)
                    if result_consecutive < temp_alcohol:
                        result_consecutive = temp_alcohol


                # if result_consecutive <  temp_alcohol:
                #     result_consecutive = temp_alcohol
                # if result_consecutive < max(temp_list_consecutive_with_alcohol):
                #     result_consecutive = max(temp_list_consecutive_with_alcohol)
                #     temp_list_consecutive_with_alcohol.clear()
                #     temp_list_consecutive_with_alcohol.append(0)
                    # print(result_consecutive)


                # print(temp_list_consecutive_with_alcohol)

                # result_consecutive = max(temp_list_consecutive_with_alcohol)
            return result_consecutive
            # return max(temp_list_consecutive_with_alcohol)
        except Exception as e:
            print(e)
            return 0

    def get_consecutive_without_alcohol(self, amount_day, end_date):
        try:
            temp_without_alcohol = 0
            temp_list_consecutive_without_alcohol = list()
            result_consecutive = 0

            for item in range(0, amount_day, 1):
                current_date_loop = end_date - timedelta(days=item if item > 0 else 0)

                condition = Calendar.objects.filter(user=self.user, date_drinking=current_date_loop).exists()


                if condition is False:
                    temp_without_alcohol += 1
                elif condition is True:
                    calendar = Calendar.objects.get(user=self.user, date_drinking=current_date_loop)
                    if calendar.is_drinked == False:
                        temp_without_alcohol += 1
                    else:
                        if temp_without_alcohol > 0:
                            if result_consecutive < temp_without_alcohol:
                                result_consecutive = temp_without_alcohol
                            # temp_list_consecutive_without_alcohol.append(temp_without_alcohol)
                        temp_without_alcohol = 0

                if item+1 == amount_day and temp_without_alcohol > 0:
                    # temp_list_consecutive_without_alcohol.append(temp_without_alcohol)
                    if result_consecutive < temp_without_alcohol:
                        result_consecutive = temp_without_alcohol

            # temp_list_consecutive_without_alcohol.append(condition_without_alcohol)
            return result_consecutive
            # return max(temp_list_consecutive_without_alcohol)
        except Exception as e:
            print(e)
            return 0

    def get_all_time(self, ):
        start_object_date = Calendar.objects.filter(user=self.user, is_drinked=True).order_by('date_drinking')[:1]
        start_date = start_object_date[0].date_drinking
        end_date = datetime.today().date()
        end_date = end_date if (end_date - self.user.date_second_login.date()).days + 1 < 14\
            else self.user.date_second_login.date()

        if not self._exist_drinking_day(start_date, end_date):
            return {
                'calendar_empty': True,
                'date_starting_drinking': 0,
                'is_often_drinking': False,
                'count_often_drinking': 0,
                'with_alcohol': 0,
                'without_alcohol': 0,
                'amount_day': 0,
                'consecutive_with_alcohol': 0,
                'consecutive_without_alcohol': 0,
            }

        amount_day = (end_date - start_date).days + 1
        amount_day = 1 if amount_day <= 0 else amount_day

        with_alcohol = Calendar.objects.filter(user=self.user, is_drinked=True,
                                               date_drinking__range=[start_date, end_date]).count()

        without_alcohol = amount_day - with_alcohol

        is_often_drinking, count_often_drinking = self._get_percent(start_date, end_date, with_alcohol)

        return {
            'calendar_empty': False,
            'date_starting_drinking': str(start_object_date[0].date_drinking.strftime('%Y-%m-%d')),
            'is_often_drinking': is_often_drinking,
            'count_often_drinking': count_often_drinking,
            'with_alcohol': with_alcohol,
            'without_alcohol': 0 if without_alcohol < 0 else without_alcohol,
            'amount_day': amount_day,
            'consecutive_with_alcohol': self.get_consecutive_with_alcohol(amount_day, end_date),
            'consecutive_without_alcohol': self.get_consecutive_without_alcohol(amount_day, end_date),
        }

    def get_thirty_days(self):
        start_date = datetime.today().date() - timedelta(days=30)
        end_date = datetime.today().date()
        end_date = end_date if (end_date - self.user.date_second_login.date()).days + 1 < 14 \
            else self.user.date_second_login.date()

        if not self._exist_drinking_day(start_date, end_date):
            return {
                'calendar_empty': True,
                'is_often_drinking': False,
                'count_often_drinking': 0,
                'with_alcohol': 0,
                'without_alcohol': 0,
                'consecutive_with_alcohol': 0,
                'consecutive_without_alcohol': 0,
            }

        with_alcohol, without_alcohol = self._get_drinking_list(start_date, end_date)

        is_often_drinking, count_often_drinking = self._get_percent(start_date, end_date, with_alcohol)

        amount_day = (end_date - start_date).days
        amount_day = 1 if amount_day <= 0 else amount_day

        return {
            'calendar_empty': False,
            'is_often_drinking': is_often_drinking,
            'count_often_drinking': count_often_drinking,
            'with_alcohol': with_alcohol,
            'without_alcohol': 0 if without_alcohol < 0 else without_alcohol,
            'consecutive_with_alcohol': self.get_consecutive_with_alcohol(amount_day, end_date),
            'consecutive_without_alcohol': self.get_consecutive_without_alcohol(amount_day, end_date),
        }

    def get_current_month(self):
        start_date = datetime.today().replace(day=1).date()
        end_date = datetime.today().date()


        if not self._exist_drinking_day(start_date, end_date):
            return {
                'calendar_empty': True,
                'is_often_drinking': False,
                'count_often_drinking': 0,
                'with_alcohol': 0,
                'without_alcohol': 0,
                'consecutive_with_alcohol': 0,
                'consecutive_without_alcohol': 0,
            }

        # if self.user.date_second_login.date().month < start_date.month:
        #     with_alcohol, without_alcohol = 0, (end_date - start_date).days + 1
        # else:
        #     with_alcohol, without_alcohol = self._get_drinking_list(start_date, end_date)

        with_alcohol, without_alcohol = self._get_drinking_list(start_date, end_date)

        is_often_drinking, count_often_drinking = self._get_percent(start_date, end_date, with_alcohol)

        amount_day = (end_date - start_date).days + 1
        amount_day = 1 if amount_day <= 0 else amount_day

        return {
            'calendar_empty': False,
            'is_often_drinking': is_often_drinking,
            'count_often_drinking': count_often_drinking,
            'with_alcohol': with_alcohol,
            'without_alcohol': 0 if without_alcohol < 0 else without_alcohol,
            'consecutive_with_alcohol': self.get_consecutive_with_alcohol(amount_day, end_date),
            'consecutive_without_alcohol': self.get_consecutive_without_alcohol(amount_day, end_date),
        }

    def get_previous_month(self):

        start_date = datetime.today().replace(day=1).date() + relativedelta(months=-1)
        end_date = start_date + relativedelta(months=1) - timedelta(days=1)

        if not self._exist_drinking_day(start_date, end_date):
            return {
                'calendar_empty': True,
                'is_often_drinking': False,
                'count_often_drinking': 0,
                'with_alcohol': 0,
                'without_alcohol': 0,
                'consecutive_with_alcohol': 0,
                'consecutive_without_alcohol': 0,
            }

        with_alcohol, without_alcohol = self._get_drinking_list(start_date, end_date)

        is_often_drinking, count_often_drinking = self._get_percent(start_date, end_date, with_alcohol)

        amount_day = (end_date - start_date).days + 1
        amount_day = 1 if amount_day <= 0 else amount_day

        return {
            'calendar_empty': False,
            'is_often_drinking': is_often_drinking,
            'count_often_drinking': count_often_drinking,
            'with_alcohol': with_alcohol,
            'without_alcohol': 0 if without_alcohol < 0 else without_alcohol,
            'consecutive_with_alcohol': self.get_consecutive_with_alcohol(amount_day, end_date),
            'consecutive_without_alcohol': self.get_consecutive_without_alcohol(amount_day, end_date),
        }

    def get_current_year(self):
        start_date = datetime(datetime.today().now().year, 1, 1).date()
        # end_date = datetime(datetime.today().now().year,12,31)
        end_date = datetime.today().date()
        end_date = end_date if (end_date - self.user.date_second_login.date()).days + 1 < 14 \
            else self.user.date_second_login.date()

        if not self._exist_drinking_day(
                start_date, datetime(datetime.today().now().year, 1, 31).date()):
            return {
                'calendar_empty': True,
                'is_often_drinking': False,
                'count_often_drinking': 0,
                'with_alcohol': 0,
                'without_alcohol': 0,
                'consecutive_with_alcohol': 0,
                'consecutive_without_alcohol': 0,
            }

        with_alcohol, without_alcohol = self._get_drinking_list(start_date, end_date)

        is_often_drinking, count_often_drinking = self._get_percent(start_date, end_date, with_alcohol)

        amount_day = (end_date - start_date).days
        amount_day = 1 if amount_day <= 0 else amount_day

        return {
            'calendar_empty': False,
            'is_often_drinking': is_often_drinking,
            'count_often_drinking': count_often_drinking,
            'with_alcohol': with_alcohol,
            'without_alcohol': without_alcohol,
            'consecutive_with_alcohol': self.get_consecutive_with_alcohol(amount_day, end_date),
            'consecutive_without_alcohol': self.get_consecutive_without_alcohol(amount_day, end_date),
        }

    def _exist_drinking_day(self, start_date, end_date):
        if Calendar.objects.filter(user=self.user, is_drinked=True,
                                               date_drinking__range=[start_date, end_date]):
            return True
        return False

    def _check_drinking_day(self):
        if not Calendar.objects.filter(user=self.user, is_drinked=True).exists():
            return False
        return True


    def call(self, user_info):
        # user_info = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        self.user = User.objects.get(username=user_info['username'])

        if not self._check_drinking_day():
            return {
                'calendar_empty': True,
                'message': 'Нет отметок в календаре',
                'status_code': 204,
                'status': 'Success'}

        return {
            'date_register': self.user.date_register.date().strftime('%Y-%m-%d'),
            'all_time': self.get_all_time(),
            'thirty_days': self.get_thirty_days(),
            'current_month': self.get_current_month(),
            'previous_month': self.get_previous_month(),
            'current_year': self.get_current_year(),
            'calendar_empty': False,
            'message': 'Статистика собрана',
            'status_code': 200,
            'status': 'Success'}
