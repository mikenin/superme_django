import jwt

from models.calendar import Calendar
from models.user import User
from service.generate_calendar import GenerateCalendar
from superme_django import settings
from superme_django.constants import TRANSLATE_MONTH


class GetCalendarService:

    def call(self,):
        try:
            # user_info = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
            # user = User.objects.get(username=user_info['username'])

            get_general_calendar = GenerateCalendar()
            general_calendar = get_general_calendar.generate()
            # general_calendar = get_general_calendar.generate(user.date_register)

            return {'message': 'Календарь подготовлен',
                    'status_code': 200,
                    'status': 'Success',
                    'general_calendar': general_calendar,
                    }
        except:
            return {'message': 'Ошибка подготовки календаря на сервере, попробуйте позже',
                    'status_code': 400,
                    'status': 'Failed',
                    'general_calendar': None,
                    }
