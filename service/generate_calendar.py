import calendar
from datetime import datetime, date
from dateutil.rrule import rrule, MONTHLY

from superme_django.constants import TRANSLATE_MONTH


class GenerateCalendar:

    def __init__(self):
        self.now = datetime.now()

    def months(self, start_month, start_year, end_month, end_year):
        start = datetime(start_year, start_month, 1)
        end = datetime(end_year, end_month, 1)
        return [(d.month, d.year) for d in rrule(MONTHLY, dtstart=start, until=end)]

    # def generate(self, user_register):
    def generate(self, ):
        if (self.now.date().month in [12,11,10,9] ):
            end_month = 3
            end_year = self.now.date().year + 1
        else:
            end_month = self.now.date().month + 3
            end_year = self.now.date().year

        month_list = self.months(1, self.now.date().year-1, 12, self.now.date().year)
        result_calendar = dict()

        for item in month_list:
            result_calendar[
                    str(item[0]) + "." + str(item[1])] = calendar.monthcalendar(item[1], item[0])

        return result_calendar