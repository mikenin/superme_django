import jwt

from api.v1.serializers.manage_push import ManagePushSerializer
from models.push_notification import PushNotification
from models.user import User
from superme_django import settings


class ManagePushForUserService:

    def validate_data(self, data):
        serializer = ManagePushSerializer(data=data)
        result_serializer = serializer.is_valid()
        if result_serializer:
            return True
        return False


    def call(self, data, token):
        try:

            if not self.validate_data(data):
                return {'message': 'Данные не валидны',
                    'status_code': 400,
                    'status': 'Failed'}

            user_info = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])

            if not PushNotification.objects.filter(user__username=user_info['username']).exists():
                PushNotification.objects.create(
                    user=User.objects.get(username = user_info['username']),
                    uid_smartphone=data['uid_smartphone'],
                    is_push=data['is_push']
                )
            else:
                push = PushNotification.objects.get(user__username=user_info['username'])
                push.uid_smartphone = data['uid_smartphone']
                push.is_push = data['is_push']
                push.save()


            # PushNotification.objects.update_or_create(
            #     user__username=user_info['username'],
            #     uid_smartphone = data['uid_smartphone'],
            #     defaults={
            #         'user': User.objects.get(username = user_info['username']),
            #         'uid_smartphone': data['uid_smartphone'],
            #         'token_firebase': data['token_firebase'],
            #         'is_push': data['is_push'],
            #
            #     }
            # )

            return {'message': 'Статус пушей обновлен',
                    'status_code': 200,
                    'status': 'Success'}
        except:
            return {'message': 'Ошибка смены статуса',
                    'status_code': 400,
                    'status': 'Failed'}
