from rest_framework.response import Response
from rest_framework.views import APIView

from service.custom_is_auth import CustomIsAuthenticated
from service.save_status_drinking import SaveStatusDrinkingService

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema


class SaveStatusDrinking(APIView):
    permission_classes = [CustomIsAuthenticated, ]

    @swagger_auto_schema(
        operation_summary='Присвоение статуса выпивки в конкретный день',
        operation_description="Нужен токен",
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'date_drinking': openapi.Schema(type=openapi.TYPE_STRING, description='05.02.2021'),
                'is_drinked': openapi.Schema(type=openapi.TYPE_BOOLEAN, description='False'),
            }
        ),
        responses={
            "200": openapi.Response(
                description="Успешный возврат статистики",
                examples={
                    "application/json": {'message': 'Статус дня обновлен',
                                         'status_code': 200,
                                         'status': 'Success'}
                }
            ),
            "400": openapi.Response(
                description="Успешный возврат баннера",
                examples={
                    "application/json": {'message': 'Ошибка обновления',
                                         'status_code': 400,
                                         'status': 'Failed'}
                }
            ),

        }
    )
    def post(self, request):
        token = request.headers['Authorization'][7:]

        save_drinking = SaveStatusDrinkingService()
        result = save_drinking.call(request.data, token)

        return Response({'status': result['status'], 'message': result['message']}, status=result['status_code'])
