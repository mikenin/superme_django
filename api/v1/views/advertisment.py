import os

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response
from rest_framework.views import APIView

from models.advertisment import AdvertismentBanner
from service.custom_is_auth import CustomIsAuthenticated



class Advertisement(APIView):
    permission_classes = [CustomIsAuthenticated, ]

    @swagger_auto_schema(
        operation_summary='Возврат рекламного баннера',
        operation_description="Нужен токен",
        # request_body=UserRegistrationSerializer,
        responses={
            "200": openapi.Response(
                description="Успешный возврат баннера",
                examples={
                    "application/json": {
                        "message": "Рекламный баннер получен",
                        "text_button": "Попробовать йогу",
                        "link_redirect": "",
                        "link": "http://localhost:8000/media/banner/Sl1OmZNWIuw.jpg",
                        "status_code": 200,
                        "status": "Success"
                    }
                }
            ),
            "204": openapi.Response(
                description='',
                examples={
                    "application/json": {
                    'message': 'Нет контента',
                    'text_button': '',
                    'link': '',
                    'status_code': 204,
                    'status': 'Success'}
                }
            ),
            "400": openapi.Response(
                description='',
                examples={
                    "application/json": {
                        'message': 'Статистика не собрана из-за проблем на сервере. Повторите позже',
                        'status_code': 400,
                        'status': 'Failed'
                    }
                }
            ),


        }
    )



    def get(self, request):
        try:
            result = AdvertismentBanner.objects.filter(is_publish=True).first()

            if not result:
                return Response({
                    'message': 'Нет контента',
                    'text_button': '',
                    'link': '',
                    'status_code': 204,
                    'status': 'Success'}, status=204)

            return Response({
                'message': 'Рекламный баннер получен',
                'text_button': result.text_button,
                'link_redirect': result.link,
                'link': os.getenv('SITE_HOST', 'http://localhost:8000') + '/media/' + str(result.banner),
                'status_code': 200,
                'status': 'Success'})

        except Exception as e:
            print(e)
            return Response({
                'message': 'Статистика не собрана из-за проблем на сервере. Повторите позже',
                'status_code': 400,
                'status': 'Failed'}, status=400)