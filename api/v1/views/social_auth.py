from rest_framework.response import Response
from rest_framework.views import APIView

from api.v1.serializers.register_user import RegisterUserSerializer

from service.custom_is_auth import CustomIsAuthenticated
from service.social_auth import SocialAuthService

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema


class SocialAuth(APIView):
    permission_classes = [CustomIsAuthenticated, ]

    @swagger_auto_schema(
        operation_summary='Авторизация через Эппл/Андроид',
        operation_description="Нужен токен",
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'username': openapi.Schema(type=openapi.TYPE_STRING, description=''),
                'device_id': openapi.Schema(type=openapi.TYPE_STRING, description='123aaas'),
                'email': openapi.Schema(type=openapi.TYPE_STRING, description='2mail@mail.com'),
                'first_name': openapi.Schema(type=openapi.TYPE_STRING, description='Vanya'),
                'last_name': openapi.Schema(type=openapi.TYPE_STRING, description='Kopez'),
                'id_account_network': openapi.Schema(type=openapi.TYPE_STRING, description=''),
            }
        ),
        responses={
            "200": openapi.Response(
                description="",
                examples={
                    "application/json": {
                        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IjJtYWlsQG1haWwuY29tIiwiZGV2aWNlX2lkIjoiMTIzYWFhcyJ9.dpzICzcFIKEYnE86hsBOB48b5c7nmqqMlPlx9C744rk",
                        "message": "Успешно авторизован",
                        "status_code": 200,
                        "status": "Success"
                    }
                }
            ),
            "400": openapi.Response(
                description="",
                examples={
                    "application/json": {
                        'message': 'Не хвататет полей',
                        'status_code': 400,
                        'status': 'Failed'}
                }
            ),
            "500": openapi.Response(
                description="",
                examples={
                    "application/json": {
                        'message': 'Проблема с авторизацией',
                        'status_code': 500,
                        'status': 'Failed'}
                }
            ),

        }
    )
    def post(self, request):
        try:
            token = request.headers['Authorization'][7:]

            # serializer = RegisterUserSerializer(data=request.data)
            # result_serializer = serializer.is_valid()
            # if result_serializer:
            #     validated_data = dict(serializer.validated_data)
            auth = SocialAuthService()
            result = auth.call(request.data, token)
            return Response(result)
            # else:
            #     return Response({
            #         'message': 'Не хвататет полей',
            #         'status_code': 400,
            #         'status': 'Failed'}, status=400)


        except Exception as e:
            print(e)
            return Response({
                'message': 'Проблема с авторизацией',
                'status_code': 500,
                'status': 'Failed'}, status=500)
