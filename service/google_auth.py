import jwt

from models.user import User
from superme_django import settings
from models.socialnetwork_account import SocialnetworkAccount
from superme_django.constants import TYPE_SOCIALNETWORK, GOOGLE
from django.utils import timezone

class GoogleAuthService:

    def call(self, data, token):
        user_info = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        user = User.objects.get(username=user_info['username'])



        if not SocialnetworkAccount.objects.filter(user=user, type_socialnetwork=GOOGLE).exists():
            network = SocialnetworkAccount()
            network.type_socialnetwork = GOOGLE
            network.user = user
            network.id_account_network = data['id_account_network']
            network.first_name =  data['first_name']
            network.last_name = data['last_name']
            network.save()

            user.email = data['email']
            user.first_name = data['first_name']
            user.last_name = data['last_name']
            user.save()

            return {
                    'message': 'Пользователь успешно авторизован через Google',
                    'status_code': 200,
                    'status': 'Success'}
        else:
            network = SocialnetworkAccount.objects.get(user=user, type_socialnetwork=GOOGLE)
            network.date_updated = timezone.now()
            network.save()

        return {
            'message': 'У пользователя уже есть Google аккаунт в системе',
            'status_code': 200,
            'status': 'Success'}