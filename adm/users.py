from django.contrib import admin
from models.user import User




@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['username', 'first_name', 'last_name', 'date_login', 'date_register',
                    ]
    list_filter = ['date_login', 'date_register', ]
    readonly_fields = ['date_login', 'date_register', 'date_refresh']
    ordering = ('-date_register',)

    fieldsets = (
        ('Персональная информация', {
            'fields': ('username', 'email', 'first_name', 'last_name', 'device_id')
        }),
        ('Статус пользователя', {
            'fields': (
            'is_active', 'is_staff', 'is_superuser')
        }),
        ('Посещаемость', {
            'fields': ('date_register', 'date_login', 'date_refresh', 'date_second_login')
        }),

    )


    class Meta:
        model = User