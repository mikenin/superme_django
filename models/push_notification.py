from django.db import models
import uuid


class PushNotification(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    uid_smartphone = models.CharField(max_length=500, verbose_name='UID-устройства', db_index=True)

    is_push = models.BooleanField(default=False, verbose_name='Отправлять пуш')
    user = models.ForeignKey('User', related_name='push_user', on_delete=models.CASCADE, verbose_name='Пользователь')

    date_created = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')

    def __str__(self):
        return self.uid_smartphone

    class Meta:
        verbose_name = "Устройство пользователя"
        verbose_name_plural = "Устройства пользователей"
