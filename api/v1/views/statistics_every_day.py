from rest_framework.response import Response
from rest_framework.views import APIView

from service.custom_is_auth import CustomIsAuthenticated

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from service.statistics_for_banner import StatisticsForBannerService


class StatisticsEveredayPopup(APIView):
    permission_classes = [CustomIsAuthenticated, ]

    @swagger_auto_schema(
        operation_summary='Возврат статистики для баннера',
        operation_description="Нужен токен",
        responses={
            "200": openapi.Response(
                description="Успешный возврат статистики",
                examples={
                    "application/json": {
                        "message": "Статистика успешно собрана",
                        "status": "Success",
                        "status_code": 200,
                        "result": {
                            "consecutive_with_alcohol": 1,
                            "consecutive_without_alcohol": 0,
                        }
                    }
                }
            ),
            "204": openapi.Response(
                description='',
                examples={
                    "application/json":
                        {
                            'statistics': True,
                            'message': 'Нет отметок в календаре',
                            'status_code': 204,
                            'status': 'Success'
                        }
                }
            ),
            "400": openapi.Response(
                description='',
                examples={
                    "application/json": {
                        'message': 'Статистика не собрана из-за проблем на сервере. Повторите позже',
                        'status_code': 400,
                        'status': 'Failed'}
                }
            ),

        }
    )
    def get(self, request):
        try:
            token = request.headers['Authorization'][7:]

            statistics = StatisticsForBannerService()
            result = statistics.call(token)

            return Response(result, status=result['status_code'])

        except Exception as e:
            print(e)
            return Response({
                'message': 'Статистика не собрана из-за проблем на сервере. Повторите позже',
                'status_code': 400,
                'status': 'Failed'}, status=400)
