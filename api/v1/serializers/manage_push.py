from rest_framework import serializers


class ManagePushSerializer(serializers.Serializer):
    uid_smartphone = serializers.CharField(required=True)
    is_push = serializers.BooleanField(required=True)