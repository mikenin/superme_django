from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.v1.serializers.register_user import RegisterUserSerializer
from models.socialnetwork_account import SocialnetworkAccount
from models.user import User
from django.utils import timezone

from service.custom_is_auth import CustomIsAuthenticated
from service.google_auth import GoogleAuthService


class GoogleAuth(APIView):
    permission_classes = [CustomIsAuthenticated, ]

    def post(self, request):
        try:
            token = request.headers['Authorization'][7:]

            serializer = RegisterUserSerializer(data=request.data)
            result_serializer = serializer.is_valid()
            if result_serializer:
                validated_data = dict(serializer.validated_data)
                auth = GoogleAuthService()
                result = auth.call(validated_data, token)
                return Response(result)
            else:
                return Response({
                    'message': 'Не хвататет полей',
                    'status_code': 400,
                    'status': 'Failed'})


        except Exception as e:
            print(e)
            return Response({
                'message': 'Проблема добавления Google аккаунта',
                'status_code': 400,
                'status': 'Failed'})