APPLE = 'Apple'
GOOGLE = 'Google'
ANONIM = 'Anonymous'


TYPE_SOCIALNETWORK = (
    (APPLE, 'Apple'),
    (GOOGLE, 'Google'),
    (ANONIM, 'Anonymous'),
)


TRANSLATE_MONTH = {
    1: "Январь",
    2: "Февраль",
    3: "Март",
    4: "Апрель",
    5: "Май",
    6: "Июнь",
    7: "Июль",
    8: "Август",
    9: "Сентябрь",
    10: "Октябрь",
    11: "Ноябрь",
    12: "Декабрь",
}

ALL = 'ALL'
YEAR = 'YEAR'
THIRTY = 'THIRTY'
PREV = 'PREV'
CURR = 'CURR'


TYPE_STATISTICS = (
    (ALL, 'ALL'),
    (YEAR, 'YEAR'),
    (THIRTY, 'THIRTY'),
    (PREV, 'PREV'),
    (CURR, 'CURR'),
)