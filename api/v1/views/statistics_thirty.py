from rest_framework.response import Response
from rest_framework.views import APIView

from models.user import User
from service.custom_is_auth import CustomIsAuthenticated
from service.redis import RedisService
from service.statistics import StatisticsService

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from superme_django import settings
import jwt

class StatisticsThirty(APIView):
    permission_classes = [CustomIsAuthenticated, ]

    def get(self, request):
        try:
            token = request.headers['Authorization'][7:]
            user_info = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])

            redis = RedisService()
            key = "thirty_{}".format(user_info['username'])
            statistics_cache = redis.get(key)
            result = None


            if statistics_cache and not statistics_cache['need_update']:
                print('cache')
                result = statistics_cache['data']

            if not statistics_cache or statistics_cache['need_update']:
                print('clear')
                statistics = StatisticsService()
                statistics.user = User.objects.get(username=user_info['username'])
                result = statistics.get_thirty_days()
                redis.set(key, result)

            return Response({"thirty_days": result,
                             "status_code": 200,
                             "status": "Success"
                             }, status=200)

        except Exception as e:
            print(e)
            return Response({
                'message': 'Статистика не собрана из-за проблем на сервере. Повторите позже',
                'status_code': 400,
                'status': 'Failed'}, status=400)
