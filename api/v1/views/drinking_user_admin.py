from rest_framework.response import Response
from rest_framework.views import APIView

from models.calendar import Calendar
from rest_framework.permissions import IsAdminUser




class DrinkingUserAdmin(APIView):
    permission_classes = [IsAdminUser,]

    def get(self, request, username):
        try:
            if not Calendar.objects.filter(user__username=username, is_drinked=True).exists:
                return Response({'status': 204,
                             'message': 'Empty',
                             'drinking': [],
                             },
                            status=204)


            result = Calendar.objects.filter(user__username=username, is_drinked=True).values_list('date_drinking', flat=True)

            return Response({'status': 200,
                             'message': 'All data good extract',
                             'drinking': [ item.strftime("%m-%d-%Y") for item in result ],
                             },
                            status=200)
        except Exception as e:
            print(e)
            return Response({'status': 500,
                             'message': 'problem server',
                             'drinking': [],
                             },
                            status=500)
