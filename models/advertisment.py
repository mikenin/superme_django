from django.db import models
import uuid


class AdvertismentBanner(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    name_banner = models.CharField(max_length=255, verbose_name='Имя баннера')
    banner = models.ImageField(upload_to='banner', verbose_name='Баннер')
    text_button = models.CharField(max_length=255, verbose_name='Текст кнопки', default='')
    link = models.TextField(verbose_name='Ссылка для перехода', default='', help_text='https://site.ru/anywhere')

    is_publish = models.BooleanField(default=False, verbose_name='Опубликовано')

    date_created = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    date_updated = models.DateTimeField(auto_now=True, verbose_name='Дата обновления')

    def __str__(self):
        return self.name_banner

    class Meta:
        verbose_name = "Рекламный баннер"
        verbose_name_plural = "Рекламные баннеры"
