from django.contrib import admin

from models.advertisment import AdvertismentBanner


@admin.register(AdvertismentBanner)
class AdvertismentAdmin(admin.ModelAdmin):
    list_display = ['name_banner', 'is_publish', 'date_created',]
    ordering = ('-date_created',)

    class Meta:
        model = AdvertismentBanner