from api.v1.serializers.get_statistics import GetStatisticsSerializer
from models.cache_statistic import CacheStatistic
from models.user import User
from superme_django.constants import ALL, YEAR, THIRTY, PREV, CURR


class GetStatisticsService:

    def call(self, username):
        try:
            cache = CacheStatistic.objects.filter(user__username=username)

            if not cache.exists():
                return {
                    'calendar_empty': True,
                    'message': 'Нет отметок в календаре',
                    'status_code': 200,
                    'status': 'Success'
                }

            return {
                'calendar_empty': False,
                'message': 'Статистика собрана',
                'status_code': 200,
                'status': 'Success',
                "all_time": GetStatisticsSerializer(cache.get(type_statistics=ALL), many=False).data,
                "current_year": GetStatisticsSerializer(cache.get(type_statistics=YEAR), many=False).data,
                "thirty_days": GetStatisticsSerializer(cache.get(type_statistics=THIRTY), many=False).data,
                "previous_month": GetStatisticsSerializer(cache.get(type_statistics=PREV), many=False).data,
                "current_month": GetStatisticsSerializer(cache.get(type_statistics=CURR), many=False).data,
                "date_register": User.objects.get(username=username).date_register.date().strftime('%Y-%m-%d')
            }

        except Exception as e:
            print(e)
            return {
                'message': 'Статистика не собрана из-за проблем на сервере. Повторите позже',
                'status_code': 400,
                'status': 'Failed'
            }