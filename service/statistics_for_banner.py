import jwt

from models.calendar import Calendar
from models.user import User
from superme_django import settings
from datetime import datetime, timedelta


class StatisticsForBannerService:

    def _get_consecutive_with_alcohol(self, amount_day, end_date, user):
        try:
            temp_alcohol = 0
            for item in range(0, amount_day, 1):
                current_date_loop = end_date - timedelta(days=item if item > 0 else 0)
                if Calendar.objects.filter(user=user, date_drinking=current_date_loop, is_drinked=True).exists():
                    temp_alcohol += 1
                else:
                    return temp_alcohol
            return temp_alcohol
        except:
            return 0

    def _get_consecutive_without_alcohol(self, amount_day, end_date, user):
        try:
            temp_without_alcohol = 0

            for item in range(0, amount_day, 1):
                current_date_loop = end_date - timedelta(days=item if item > 0 else 0)
                condition = Calendar.objects.filter(user=user, date_drinking=current_date_loop).exists()

                if condition is False:
                    temp_without_alcohol += 1
                elif condition is True:
                    calendar = Calendar.objects.get(user=user, date_drinking=current_date_loop)
                    if calendar.is_drinked == False:
                        temp_without_alcohol += 1
                    else:
                        return temp_without_alcohol

            return temp_without_alcohol
        except:
            return 0

    def _check_drinking_day(self, user):
        if not Calendar.objects.filter(user=user).exists():
            return False
        return True

    def call(self, token):
        user_info = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        user = User.objects.get(username=user_info['username'])
        if not self._check_drinking_day(user):
            return {
                'statistics': True,
                'message': 'Нет отметок в календаре',
                'status_code': 204,
                'status': 'Success'
            }

        start_object_date = Calendar.objects.filter(user=user).order_by('date_drinking')[:1]
        start_date = start_object_date[0].date_drinking

        end_date = datetime.today().date() - timedelta(days=1)

        amount_day = (end_date - start_date).days + 1
        amount_day = 1 if amount_day <= 0 else amount_day

        return {
            "message": "Статистика успешно собрана",
            "status": "Success",
            "status_code": 200,
            "result": {
                "consecutive_with_alcohol": self._get_consecutive_with_alcohol(amount_day=amount_day,
                                                                               end_date=end_date, user=user),
                "consecutive_without_alcohol": self._get_consecutive_without_alcohol(amount_day=amount_day,
                                                                                     end_date=end_date, user=user),
            }
        }
