from django.db import transaction

from models.cache_statistic import CacheStatistic
from models.calendar import Calendar
from models.history_calendar import HistoryCalendar


class StatisticsRefreshService:

    def _delete_cache_statistic(self, user):
        CacheStatistic.objects.filter(user=user).delete()

    def _delete_current_calendar(self, user):
        Calendar.objects.filter(user=user).delete()

    def _replace_current_calender(self, user):
        old_calendar = Calendar.objects.filter(user=user)
        for item in old_calendar:
            HistoryCalendar.objects.create(
                date_drinking=item.date_drinking,
                is_drinked=item.is_drinked,
                user=user,
                date_created=item.date_created,
                date_updated=item.date_updated
            )

    def refresh(self, user):
        try:
            with transaction.atomic():
                self._replace_current_calender(user=user)
                self._delete_current_calendar(user=user)
                self._delete_cache_statistic(user=user)
                return {
                        "message": "Статистика обнулена",
                        "status": "Success",
                        "status_code": 200,
                    }
        except Exception as e:
            print(e)
            return {
                'message': 'Статистика не собрана из-за проблем на сервере. Повторите позже',
                'status_code': 400,
                'status': 'Failed'
            }