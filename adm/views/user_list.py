from django.shortcuts import render

from api.v1.serializers.user_statistics import UserStatisticsSerializer
from models.user import User
from service.sorted_user import SortedUserService
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def user_admin_custom_page(request):
    try:
        if request.method == "GET" and request.GET.get('type_sorted') is None:

            user_list = User.objects.all().order_by('-date_register')
            serializer = UserStatisticsSerializer(user_list, many=True).data

            paginator = Paginator(serializer, 10)
            page = request.GET.get('page')

            try:
                user_list = paginator.page(page)
            except PageNotAnInteger:
                user_list = paginator.page(1)
            except EmptyPage:
                user_list = paginator.page(paginator.num_pages)

            ctx = {'data': user_list}
            return render(request, 'user_admin_custom_page.html', ctx)

        if (request.method == "POST") or (request.method == "GET" and request.GET.get('type_sorted') != None
                                          or request.GET.get('page') is not None):

            type_sorted = request.POST.get("action") if request.POST.get("action") != None else request.GET.get('type_sorted')
            sorted_user = SortedUserService()
            serializer = UserStatisticsSerializer(sorted_user.call(type_sorted), many=True).data

            paginator = Paginator(serializer, 10)

            if request.POST.get("action") != None:
                page = request.GET.get('page') if request.GET.get('type_sorted') != request.POST.get("action") else 1
            else:
                page = request.GET.get('page')

            try:
                user_list = paginator.page(page)
            except PageNotAnInteger:
                user_list = paginator.page(1)
            except EmptyPage:
                user_list = paginator.page(paginator.num_pages)

            ctx = {'data': user_list, "type_sorted": type_sorted}
            return render(request, 'user_admin_custom_page.html', ctx)

    except Exception as e:
        print(e)
        return render(request, 'user_admin_custom_page.html')


