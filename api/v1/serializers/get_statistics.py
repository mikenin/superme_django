from rest_framework import serializers
from models.cache_statistic import CacheStatistic


class GetStatisticsSerializer(serializers.Serializer):
    date_starting_drinking = serializers.SerializerMethodField()
    calendar_empty = serializers.BooleanField()
    is_often_drinking = serializers.BooleanField()
    count_often_drinking = serializers.IntegerField()
    with_alcohol = serializers.IntegerField()
    without_alcohol = serializers.IntegerField()
    amount_day = serializers.IntegerField()
    consecutive_with_alcohol = serializers.IntegerField()
    consecutive_without_alcohol = serializers.IntegerField()

    class Meta:
        model = CacheStatistic
        fields = ['date_starting_drinking', 'calendar_empty', 'is_often_drinking', 'count_often_drinking',
                  'with_alcohol', 'without_alcohol', 'amount_day', 'consecutive_with_alcohol',
                  'consecutive_without_alcohol',
                  ]
    def get_date_starting_drinking(self, obj):
        if obj.date_starting_drinking is not None:
            return obj.date_starting_drinking.strftime('%Y-%m-%d')
        return 0