from django.contrib import admin

from models.cache_statistic import CacheStatistic


@admin.register(CacheStatistic)
class CacheStatisticAdmin(admin.ModelAdmin):
    list_display = ['user', 'type_statistics', 'date_created', 'date_updated']
    ordering = ('-date_created',)

    class Meta:
        model = CacheStatistic