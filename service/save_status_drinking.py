import jwt

from models.calendar import Calendar
from models.user import User
from superme_django import settings
import datetime

from superme_django.celery import update_stiatiscs
import django_rq
    

class SaveStatusDrinkingService:
    def call(self, data, token):
        try:

            user_info = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
            print('user_info in save  status drinong')
            print(user_info)

            date_drinking = datetime.datetime.strptime(data['date_drinking'], '%d.%m.%Y').date()

            Calendar.objects.update_or_create(user__username = user_info['username'],
                                              date_drinking = date_drinking,
                                              defaults={
                                                  'date_drinking': date_drinking,
                                                  'is_drinked': data['is_drinked'],
                                                  'user': User.objects.get(username = user_info['username']),
                                              })

            django_rq.enqueue(update_stiatiscs, username=user_info['username'])
            print('return save status drinkind')
            return {'message': 'Статус дня обновлен',
                    'status_code': 200,
                    'status': 'Success'}
        except Exception as e:
            print('Error drinking save')
            print(e)
            return {'message': 'Ошибка обновления',
                    'status_code': 400,
                    'status': 'Failed'}