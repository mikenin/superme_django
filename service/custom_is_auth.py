import jwt
from rest_framework.permissions import BasePermission
from datetime import datetime, timedelta

from superme_django import settings
from rest_framework.response import Response

class CustomIsAuthenticated(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):


        # if bool(request.headers['Authorization'][7:]):
        #     token = request.headers['Authorization'][7:]
        #     user_info = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        #     end_date = datetime.strptime(user_info['timelife_token'], '%Y-%m-%d').date()
        #     current_date = datetime.today().date() + timedelta(days=3)
        #     return True if current_date < end_date else False
        # return False
        return bool(request.headers['Authorization'][7:])