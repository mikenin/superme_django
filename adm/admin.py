from django.contrib import admin

from .users import UserAdmin
from .advertisment import AdvertismentAdmin
from .calendar import CalendarAdmin
# from .socialnetwork_account import SocialnetworkAccountAdmin
from .push_notification import PushNotificationAdmin
from .history_calendar import HistoryCalendarAdmin
from .cache_statistic import CacheStatisticAdmin