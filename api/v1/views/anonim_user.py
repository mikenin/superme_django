from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

import jwt

from api.v1.serializers.register_user import RegisterUserSerializer
from models.user import User
from superme_django import settings
from django.utils import timezone

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from datetime import datetime, timedelta


class AnonimAuth(APIView):

    @swagger_auto_schema(
        operation_summary='Анонимная авторизация',
        operation_description="",
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'username': openapi.Schema(type=openapi.TYPE_STRING, description=''),
                'device_id': openapi.Schema(type=openapi.TYPE_STRING, description='123aaas'),
                'email': openapi.Schema(type=openapi.TYPE_STRING, description='2mail@mail.com'),
                'first_name': openapi.Schema(type=openapi.TYPE_STRING, description='Vanya'),
                'last_name': openapi.Schema(type=openapi.TYPE_STRING, description='Kopez'),
                'id_account_network': openapi.Schema(type=openapi.TYPE_STRING, description=''),
            }
        ),
        responses={
            "200": openapi.Response(
                description="",
                examples={
                    "application/json": {
                    'access_token': "123adfeFwefrwfrg",
                    'message': 'Успешно авторизован',
                    'status_code': 200,
                    'status': 'Success'}
                }
            ),
            "400": openapi.Response(
                description="",
                examples={
                    "application/json": {
                    'access_token': '',
                    'message': 'Не хвататет полей',
                    'status_code': 400,
                    'status': 'Failed'}
                }
            ),
            "500": openapi.Response(
                description="",
                examples={
                    "application/json": {
                'access_token': "",
                'message': 'Проблема авторизации',
                'status_code': 500,
                'status': 'Failed'}
                }
            ),

        }
    )

    def post(self, request):
        username = str()
        try:
            serializer = RegisterUserSerializer(data=request.data)
            result_serializer = serializer.is_valid()
            if result_serializer:
                validated_data = dict(serializer.validated_data)
                print('validated_data in anonym user')
                print(validated_data)
                if not User.objects.filter(username=validated_data['username']).exists():
                    print('not in DB anonym user')
                    user = User.objects.create(
                        username = validated_data['username'],
                        device_id = validated_data['username'],
                        email = validated_data['email'],
                        first_name = validated_data['first_name'],
                        last_name = validated_data['last_name'],
                    )
                    username = user.username
                else:
                    print('have in DB anon user')
                    user = User.objects.get(username=validated_data['username'])

                    username = user.username
                    user.date_second_login = user.date_login
                    user.save()

                # encoded = jwt.encode({"username": username, "device_id": username, "timelife_token": str(datetime.today().date() + timedelta(days=2))}, settings.SECRET_KEY, algorithm="HS256")
                encoded = jwt.encode({"username": username, "device_id": username, 'time_rand': str(datetime.utcnow())}, settings.SECRET_KEY, algorithm="HS256")
                print('username for anonym user')
                print(username)
                print('return token for anonym user')
                return Response({
                    'access_token': encoded,
                    'message': 'Успешно авторизован',
                    'status_code': 200,
                    'status': 'Success'}, status=200)
            else:
                return Response({
                    'access_token': '',
                    'message': 'Не хвататет полей',
                    'status_code': 400,
                    'status': 'Failed'}, status=400)


        except Exception as e:
            print(e)
            return Response({
                'access_token': "",
                'message': 'Проблема авторизации',
                'status_code': 500,
                'status': 'Failed'}, status=500)