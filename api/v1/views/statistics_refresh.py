import jwt
from rest_framework.response import Response
from rest_framework.views import APIView

from models.user import User
from service.custom_is_auth import CustomIsAuthenticated

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from service.statistics_refresh import StatisticsRefreshService
from superme_django import settings
from datetime import datetime

class StatisticsRefresh(APIView):
    permission_classes = [CustomIsAuthenticated, ]

    @swagger_auto_schema(
        operation_summary='Обнуление статистики пользователя',
        operation_description="Нужен токен",
        responses={
            "200": openapi.Response(
                description="Статистика обнулена",
                examples={
                    "application/json": {
                        "message": "Статистика обнулена",
                        "status": "Success",
                        "status_code": 200,
                    }
                }
            ),
            "400": openapi.Response(
                description='',
                examples={
                    "application/json": {
                        'message': 'Статистика не собрана из-за проблем на сервере. Повторите позже',
                        'status_code': 400,
                        'status': 'Failed'}
                }
            ),

        }
    )
    def get(self, request):
        token = request.headers['Authorization'][7:]
        user_info = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        user = User.objects.get(username=user_info['username'])
        user.date_refresh = datetime.today().date()
        user.save()

        statistic = StatisticsRefreshService()
        result = statistic.refresh(user=user)

        return Response(result, status=result['status_code'])
