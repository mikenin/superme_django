from django.contrib import admin

from models.history_calendar import HistoryCalendar


@admin.register(HistoryCalendar)
class HistoryCalendarAdmin(admin.ModelAdmin):
    list_display = ['date_drinking', 'user', 'is_drinked',]
    ordering = ('-date_drinking',)

    class Meta:
        model = HistoryCalendar