from django.contrib import admin

from models.calendar import Calendar


@admin.register(Calendar)
class CalendarAdmin(admin.ModelAdmin):
    list_display = ['date_drinking', 'user', 'is_drinked',]
    ordering = ('-date_drinking',)

    class Meta:
        model = Calendar