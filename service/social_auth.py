import jwt

from models.user import User
from superme_django import settings
from datetime import datetime, timedelta

class SocialAuthService:
    def call(self, data, token):
        username = str()
        user_info = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        condition_one = User.objects.filter(email=data['email'], device_id=user_info['device_id']).exists()

        if condition_one:
            encoded = jwt.encode({"username": data['email'], "device_id": data['device_id'], 'time_rand': str(datetime.utcnow())}, settings.SECRET_KEY, algorithm="HS256")
            # encoded = jwt.encode({"username": data['email'], "device_id": data['device_id'], "timelife_token": str(datetime.today().date() + timedelta(days=2))}, settings.SECRET_KEY, algorithm="HS256")
            return {
                'access_token': encoded,
                'message': 'Успешно авторизован',
                'status_code': 200,
                'status': 'Success'}

        if not condition_one:
            condition_two = User.objects.filter(device_id=data['device_id']).count()
            if condition_two == 1:
                user = User.objects.get(device_id=data['device_id'])
                if not user.email:
                    user.username = data['email']
                    user.email = data['email']
                    user.first_name = data['first_name']
                    user.last_name = data['last_name']
                    user.save()
                    username = user.username
                elif user.email and user.device_id == data['device_id'] and user.email != data['email']:
                    user = User()
                    user.username = data['email']
                    user.device_id = data['device_id']
                    user.email = data['email']
                    user.first_name = data['first_name']
                    user.last_name = data['last_name']
                    user.save()
                    username = user.username
            else:
                user = User()
                user.username = data['email']
                user.device_id = data['device_id']
                user.email = data['email']
                user.first_name = data['first_name']
                user.last_name = data['last_name']
                user.save()
                username = user.username
            encoded = jwt.encode({"username": username, "device_id": data['device_id'], 'time_rand': str(datetime.utcnow())}, settings.SECRET_KEY, algorithm="HS256")
            # encoded = jwt.encode({"username": username, "device_id": data['device_id'], "timelife_token": str(datetime.today().date() + timedelta(days=2))}, settings.SECRET_KEY, algorithm="HS256")
            return {
                'access_token': encoded,
                'message': 'Успешно авторизован',
                'status_code': 200,
                'status': 'Success'}