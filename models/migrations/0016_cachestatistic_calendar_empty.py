# Generated by Django 3.1.6 on 2021-06-22 10:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('models', '0015_cachestatistic'),
    ]

    operations = [
        migrations.AddField(
            model_name='cachestatistic',
            name='calendar_empty',
            field=models.BooleanField(default=False),
        ),
    ]
