from rest_framework import serializers

from models.cache_statistic import CacheStatistic
from models.calendar import Calendar
from models.user import User

from datetime import datetime
from service.statistics import StatisticsService
from superme_django.constants import ALL


class UserStatisticsSerializer(serializers.Serializer):
    id = serializers.UUIDField()
    username = serializers.CharField()
    device_id = serializers.CharField()
    email = serializers.CharField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    date_register = serializers.SerializerMethodField()
    date_login = serializers.SerializerMethodField()
    date_refresh = serializers.SerializerMethodField()
    amount_drinking = serializers.SerializerMethodField()
    amount_not_drinking = serializers.SerializerMethodField()
    consecutive_with_alcohol = serializers.SerializerMethodField()
    consecutive_without_alcohol = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = '__all__'

    def get_amount_not_drinking(self, obj):

        if CacheStatistic.objects.filter(user=obj, type_statistics=ALL).exists():
            cache = CacheStatistic.objects.get(user=obj, type_statistics=ALL)
            return cache.without_alcohol

        start_object_date = Calendar.objects.filter(user=obj, is_drinked=True).order_by('date_drinking')[:1]
        if start_object_date:
            start_date = start_object_date[0].date_drinking
        else:
            start_date = obj.date_register.date()

        end_date = datetime.today().date()
        try:
            end_date = end_date if (end_date - obj.date_second_login.date()).days + 1 < 14 \
                else obj.date_second_login.date()
        except:
            print('nonetype')

        amount_day = (end_date - start_date).days + 1

        amount_day = 1 if amount_day <= 0 else amount_day

        with_alcohol = Calendar.objects.filter(user=obj, is_drinked=True,
                                               date_drinking__range=[start_date, end_date]).count()
        without_alcohol = amount_day - with_alcohol

        return without_alcohol


    def get_amount_drinking(self, obj):

        if CacheStatistic.objects.filter(user=obj, type_statistics=ALL).exists():
            cache = CacheStatistic.objects.get(user=obj, type_statistics=ALL)
            return cache.with_alcohol

        return Calendar.objects.filter(is_drinked=True, user=obj).count()

    def get_date_register(self, obj):
        return obj.date_register.strftime("%d.%m.%Y %H:%M")

    def get_date_login(self, obj):
        return obj.date_login.strftime("%d.%m.%Y %H:%M")

    def get_date_refresh(self, obj):
        if obj.date_refresh:
            return obj.date_refresh.strftime("%d.%m.%Y %H:%M")
        return ''

    def get_consecutive_with_alcohol(self, obj):
        try:
            if CacheStatistic.objects.filter(user=obj, type_statistics=ALL).exists():
                cache = CacheStatistic.objects.get(user=obj, type_statistics=ALL)
                return cache.consecutive_with_alcohol

            start_object_date = Calendar.objects.filter(user=obj, is_drinked=True).order_by('date_drinking')[:1]
            if not start_object_date:
                return 0

            start_date = start_object_date[0].date_drinking
            end_date = datetime.today().date()
            try:
                end_date = end_date if (end_date - obj.date_second_login.date()).days + 1 < 14 \
                    else obj.date_second_login.date()
            except:
                print('nonetype')

            amount_day = (end_date - start_date).days + 1
            amount_day = 1 if amount_day <= 0 else amount_day
            stat = StatisticsService()
            stat.user = obj
            return stat.get_consecutive_with_alcohol(amount_day=amount_day, end_date=end_date)
        except:
            return 1112

    def get_consecutive_without_alcohol(self, obj):
        try:
            if CacheStatistic.objects.filter(user=obj, type_statistics=ALL).exists():
                cache = CacheStatistic.objects.get(user=obj, type_statistics=ALL)
                return cache.consecutive_without_alcohol

            start_object_date = Calendar.objects.filter(user=obj, is_drinked=True).order_by('date_drinking')[:1]
            if not start_object_date:
                return 0

            start_date = start_object_date[0].date_drinking
            end_date = datetime.today().date()
            try:
                end_date = end_date if (end_date - obj.date_second_login.date()).days + 1 < 14 \
                    else obj.date_second_login.date()
            except:
                print('nonetype')

            amount_day = (end_date - start_date).days + 1
            amount_day = 1 if amount_day <= 0 else amount_day
            stat = StatisticsService()
            stat.user = obj
            return stat.get_consecutive_without_alcohol(amount_day=amount_day, end_date=end_date)
        except:
            return 1113