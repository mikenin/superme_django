# Generated by Django 3.1.6 on 2021-05-26 13:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('models', '0008_user_device_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='date_refresh',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Дата обнуления'),
        ),
    ]
