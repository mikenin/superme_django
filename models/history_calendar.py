from django.db import models
import uuid


class HistoryCalendar(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    date_drinking = models.DateField(verbose_name='Дата', db_index=True)

    is_drinked = models.BooleanField(default=False, verbose_name='Пил')
    user = models.ForeignKey('User', related_name='history_calendar_user', on_delete=models.CASCADE, verbose_name='Пользователь')

    date_created = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    date_updated = models.DateTimeField(auto_now=True, verbose_name='Дата последнего изменения')

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = "История календаря пил/не пил"
        verbose_name_plural = "История календаря пил/не пил"
