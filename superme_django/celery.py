from celery import Celery
from datetime import timedelta
import os

import logging

from service.sender_push import SenderPush

import datetime


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'superme_django.settings')
app = Celery('superme_django')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks()


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(60*60, send_push.s(), name='add every hour')


@app.task(ignore_result=True)
def send_push():
    try:
        now = datetime.datetime.now()
        if now.isoweekday() == 7 and now.hour > 20 and now.hour < 21:
            sender = SenderPush()
            sender.push()
            logging.info("success send push in celery")
    except Exception as e:
        logging.warning("celery problem:", e)


def update_stiatiscs(username):
    try:
        from service.update_statistic import UpdateCacheStatistics
        cache = UpdateCacheStatistics()
        cache.call(username=username)

        print("success update")
    except Exception as e:
        print("update problem:", e)
