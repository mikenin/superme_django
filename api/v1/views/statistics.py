from rest_framework.response import Response
from rest_framework.views import APIView

from models.cache_statistic import CacheStatistic
from models.calendar import Calendar
from service.custom_is_auth import CustomIsAuthenticated
from service.get_statistics import GetStatisticsService
from service.redis import RedisService
from service.statistics import StatisticsService

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from superme_django import settings
import jwt

class Statistics(APIView):
    permission_classes = [CustomIsAuthenticated, ]

    @swagger_auto_schema(
        operation_summary='Возврат статистики',
        operation_description="Нужен токен",
        responses={
            "200": openapi.Response(
                description="Успешный возврат статистики",
                examples={
                    "application/json": {
                        "date_register": "2021-03-17",
                        "all_time": {
                            "is_often_drinking": False,
                            "count_often_drinking": 0,
                            "with_alcohol": 1,
                            "without_alcohol": 0,
                            "amount_day": 1
                        },
                        "thirty_days": {
                            "is_often_drinking": True,
                            "count_often_drinking": 0,
                            "with_alcohol": 1,
                            "without_alcohol": 29,
                            "consecutive_with_alcohol": 1,
                            "consecutive_without_alcohol": 29
                        },
                        "current_month": {
                            "is_often_drinking": True,
                            "count_often_drinking": 0,
                            "with_alcohol": 1,
                            "without_alcohol": 15,
                            "consecutive_with_alcohol": 1,
                            "consecutive_without_alcohol": 16
                        },
                        "previous_month": {
                            "is_often_drinking": False,
                            "count_often_drinking": 0,
                            "with_alcohol": 0,
                            "without_alcohol": 27,
                            "consecutive_with_alcohol": 0,
                            "consecutive_without_alcohol": 28
                        },
                        "current_year": {
                            "is_often_drinking": False,
                            "count_often_drinking": 0,
                            "with_alcohol": 1,
                            "without_alcohol": 74,
                            "consecutive_with_alcohol": 1,
                            "consecutive_without_alcohol": 74
                        },
                        "calendar_empty": False,
                        "message": "Статистика собрана",
                        "status_code": 200,
                        "status": "Success"
                    }
                }
            ),
            "204": openapi.Response(
                description='',
                examples={
                    "application/json": {
                        'calendar_empty': True,
                        'message': 'Нет отметок в календаре',
                        'status_code': 204,
                        'status': 'Success'}
                }
            ),
            "400": openapi.Response(
                description='',
                examples={
                    "application/json": {
                        'message': 'Статистика не собрана из-за проблем на сервере. Повторите позже',
                        'status_code': 400,
                        'status': 'Failed'}
                }
            ),

        }
    )
    def get(self, request):
        try:
            token = request.headers['Authorization'][7:]
            user_info = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
            if not Calendar.objects.filter(user__username=user_info['username'], is_drinked=True).exists():
                return Response({
                    'calendar_empty': True,
                    'message': 'Нет отметок в календаре',
                    'status_code': 200,
                    'status': 'Success'}, status=200)
            statistics = GetStatisticsService()
            result = statistics.call(user_info['username'])

            return Response(result, status=result['status_code'])

            # redis = RedisService()
            # key = "{}".format(user_info['username'])
            # statistics_cache = redis.get(key)
            # result = None


            # if statistics_cache and not statistics_cache['need_update']:
            #     print('cache')
            #     result = statistics_cache['data']
            #
            # if not statistics_cache or statistics_cache['need_update']:
            #     print('clear')
            #     statistics = StatisticsService()
            #     result = statistics.call(user_info)
            #     redis.set(key, result)


            # statistics = StatisticsService()
            # result = statistics.call(user_info)

            # return Response(result, status=result['status_code'])

        except Exception as e:
            print(e)
            return Response({
                'message': 'Статистика не собрана из-за проблем на сервере. Повторите позже',
                'status_code': 400,
                'status': 'Failed'}, status=400)
