import jwt
from rest_framework.response import Response
from rest_framework.views import APIView
from django.utils import timezone
from models.user import User

from service.custom_is_auth import CustomIsAuthenticated
from superme_django import settings

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema


class AuthLogin(APIView):
    permission_classes = [CustomIsAuthenticated, ]

    @swagger_auto_schema(
        operation_summary='Подтверждение входа и логин',
        operation_description="Нужен токен",
        responses={
            "200": openapi.Response(
                description="",
                examples={
                    "application/json": {
                        "message": "Время логина учтено",
                        "status_code": 200,
                        "status": "Success"
                    }
                }
            ),
            "400": openapi.Response(
                description="",
                examples={
                    "application/json": {
                        'message': 'Проблема с логином',
                        'status_code': 400,
                        'status': 'Failed'}
                }
            ),

        }
    )
    def get(self, request):
        try:
            token = request.headers['Authorization'][7:]
            user_info = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
            user = User.objects.get(username=user_info['username'], device_id=user_info['device_id'])
            date_login = user.date_login
            user.date_second_login = date_login
            user.date_login = timezone.now()
            user.save()
            return Response({
                'message': 'Время логина учтено',
                'status_code': 200,
                'status': 'Success'}, status=200)
        except:
            return Response({
                'message': 'Проблема с логином',
                'status_code': 400,
                'status': 'Failed'}, status=400)
