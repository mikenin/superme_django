import json

from rest_framework.response import Response
from rest_framework.views import APIView

from service.custom_is_auth import CustomIsAuthenticated
from service.get_calendar import GetCalendarService

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema


class GetCalendar(APIView):
    # permission_classes = [CustomIsAuthenticated, ]

    @swagger_auto_schema(
        operation_summary='Возврат главного календаря',
        operation_description="Если месяц заканчивается или начинается не ровно в конце и начале недели, то там цифра 0",
        responses={
            "200": openapi.Response(
                description="",
                examples={
                    "application/json": {
                        'message': 'Календарь подготовлен',
                        'status_code': 200,
                        'status': 'Success',
                        'general_calendar': {
                            '1.2021': [
                                [0,
                                 0,
                                 0,
                                 0,
                                 1,
                                 2,
                                 3], [4,
                                      5,
                                      6,
                                      7,
                                      8,
                                      9,
                                      10],
                                [11,
                                 12,
                                 13,
                                 14,
                                 15,
                                 16,
                                 17], [
                                    18,
                                    19,
                                    20,
                                    21,
                                    22,
                                    23,
                                    24
                                ],
                                [25,
                                 26,
                                 27,
                                 28,
                                 29,
                                 30,
                                 31]
                            ],
                            '2.2021': [
                                [0,
                                 0,
                                 0,
                                 0,
                                 1,
                                 2,
                                 3], [4,
                                      5,
                                      6,
                                      7,
                                      8,
                                      9,
                                      10],
                                [11,
                                 12,
                                 13,
                                 14,
                                 15,
                                 16,
                                 17], [
                                    18,
                                    19,
                                    20,
                                    21,
                                    22,
                                    23,
                                    24
                                ],
                                [25,
                                 26,
                                 27,
                                 28,
                                 29,
                                 30,
                                 0]
                            ]
                        }
                    }
                }
            ),
            "400": openapi.Response(
                description="",
                examples={
                    "application/json": {'message': 'Ошибка подготовки календаря на сервере, попробуйте позже',
                                         'status_code': 400,
                                         'status': 'Failed',
                                         'general_calendar': None,
                                         }
                }
            ),

        }
    )
    def get(self, request):
        # token = request.headers['Authorization'][7:]


        calendar = GetCalendarService()
        result = calendar.call()
        # result = calendar.call(token)

        return Response({'status': result['status'],
                         'message': result['message'],
                         'general_calendar': result['general_calendar'],
                         },
                        status=result['status_code'])
