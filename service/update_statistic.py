from models.cache_statistic import CacheStatistic
from models.user import User
from service.statistics import StatisticsService
from superme_django.celery import app
from superme_django.constants import ALL, YEAR, THIRTY, PREV, CURR


class UpdateCacheStatistics:

    def _update_all(self, user, data):
        CacheStatistic.objects.update_or_create(user=user, type_statistics=ALL,
                                                defaults=data)

    def _update_year(self, user, data):
        CacheStatistic.objects.update_or_create(user=user, type_statistics=YEAR,
                                                defaults=data)

    def _update_thity(self, user, data):
        CacheStatistic.objects.update_or_create(user=user, type_statistics=THIRTY,
                                                defaults=data)

    def _update_prev(self, user, data):
        CacheStatistic.objects.update_or_create(user=user, type_statistics=PREV,
                                                defaults=data)

    def _update_curr(self, user, data):
        CacheStatistic.objects.update_or_create(user=user, type_statistics=CURR,
                                                defaults=data)

    def call(self, username):
        try:
            print(1)
            user = User.objects.get(username=username)
            statistics = StatisticsService()
            statistics.user = user
            print(2)
            self._update_all(user=user, data=statistics.get_all_time())
            self._update_year(user=user, data=statistics.get_current_year())
            self._update_thity(user=user, data=statistics.get_thirty_days())
            self._update_prev(user=user, data=statistics.get_previous_month())
            self._update_curr(user=user, data=statistics.get_current_month())
            print(3)
        except Exception as e:
            print('Error cache',e)
